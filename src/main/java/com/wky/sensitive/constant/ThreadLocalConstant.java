package com.wky.sensitive.constant;

/**
 * 由于敏感词替换使用的是@SensitiveReplace注解，核心是用aop后置通知拿到返回的数据，但是如果是spring mvc的modelAndVie，
 * 经过视图解析器，前后端不分离，所以aop后置通知拿到的只是一个视图拼接字符串路径，拿不到要脱敏的数据，所以需要将数据的localVar中
 * <p>
 * DATA常量为数据标识
 * BOOL常量为是否脱敏标识
 *
 * @author weikaiyu
 * @version 1.0
 * @date 2022-02-11 16:36
 */
public class ThreadLocalConstant {

    public final static String DATA = "data";

    public final static String BOOL = "bool";

}
