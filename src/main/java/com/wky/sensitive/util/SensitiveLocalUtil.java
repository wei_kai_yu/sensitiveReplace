package com.wky.sensitive.util;

import com.wky.sensitive.aspect.SensitiveCoreBean;
import com.wky.sensitive.constant.ThreadLocalConstant;

import java.util.HashMap;
import java.util.Map;

/**
 * ThreadLocal 值操作工具类
 *
 * @author weikaiyu
 * @version 1.0
 * @date 2022-02-15 11:23
 */
public class SensitiveLocalUtil {


    public static ThreadLocal<Map<String, Object>> getLocalVar() {
        return SensitiveCoreBean.getLocalVar();
    }

    public static void setLocalVar(Object obj) {
        ThreadLocal<Map<String, Object>> localVar = getLocalVar();
        HashMap<String, Object> objectObjectHashMap = new HashMap<>();
        objectObjectHashMap.put(ThreadLocalConstant.DATA, obj);
        localVar.set(objectObjectHashMap);
    }

}
