package com.wky.sensitive.test;

/**
 * @author weikaiyu
 * @version 1.0
 * @date 2022-02-18 10:49
 */
public class MilkDecorator extends  CoffeeDecorator{

    public MilkDecorator(ICoffee coffee) {
        super(coffee);
    }

    @Override
    public void makeCoffee() {
        super.makeCoffee();
        this.addMilk();
    }
    private void addMilk(){
        System.out.print("加奶 ");
    }
}
