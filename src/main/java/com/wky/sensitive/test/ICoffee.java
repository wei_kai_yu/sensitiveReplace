package com.wky.sensitive.test;

/**
 * @author weikaiyu
 * @version 1.0
 * @date 2022-02-18 10:45
 */
public interface ICoffee {
    void makeCoffee();
}
