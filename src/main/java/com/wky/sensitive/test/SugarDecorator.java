package com.wky.sensitive.test;

/**
 * @author weikaiyu
 * @version 1.0
 * @date 2022-02-18 10:50
 */
public class SugarDecorator extends CoffeeDecorator {

    public SugarDecorator(ICoffee coffee) {
        super(coffee);
    }

    @Override
    public void makeCoffee() {
        super.makeCoffee();
        this.addSugar();
    }

    private void addSugar() {
        System.out.print("加糖 ");
    }
}
