package com.wky.sensitive.test;

/**
 * 奶茶装饰器
 * @author weikaiyu
 * @version 1.0
 * @date 2022-02-18 10:46
 */
public abstract  class CoffeeDecorator implements  ICoffee{
    private  ICoffee coffee;

    public  CoffeeDecorator(ICoffee coffee){
        this.coffee=coffee;
    }

    @Override
    public void makeCoffee() {
        coffee.makeCoffee();
    }
}
