package com.wky.sensitive.test;

/**
 * 原味
 * @author weikaiyu
 * @version 1.0
 * @date 2022-02-18 10:46
 */
public class OriginalCoffee implements  ICoffee{
    @Override
    public void makeCoffee() {
        System.out.print("原味奶茶 ");
    }
}
