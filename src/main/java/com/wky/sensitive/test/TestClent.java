package com.wky.sensitive.test;

/**
 * @author weikaiyu
 * @version 1.0
 * @date 2022-02-18 10:52
 */
public class TestClent {
    public static void main(String[] args) {
        // 原味
        ICoffee coffee = new OriginalCoffee();
        coffee.makeCoffee();
        System.out.println("");

        coffee = new MilkDecorator(coffee);
        coffee.makeCoffee();
        System.out.println("");

        coffee = new SugarDecorator(coffee);
        coffee.makeCoffee();


    }
}
