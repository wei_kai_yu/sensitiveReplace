package com.wky.sensitive.rule;

/**
 * 所有规则均实现该接口，在核心类SensitiveCoreBean中替换敏感数据时，
 * 将会根据@Replace注解配置的rulePath属性（类路径）决定调用哪个规则进行敏感数据替换
 * <p>
 * 若需要自定义规则，则实现Rule接口，并把接口的Class类注入@Replace的rulePath属性即可
 *
 * @author weikaiyu
 * @version 1.0
 * @date 2022-02-11 9:15
 */
public interface Rule {
    /**
     * 过滤接口
     *
     * @param param
     * @return
     */
    String processor(String param);

}
