package com.wky.sensitive.rule.abstracts;


import com.wky.sensitive.rule.Rule;

/**
 * 前置处理器，交由使用者调用
 *
 * @author weikaiyu
 * @version 1.0
 * @date 2022-02-11 16:09
 */
public abstract class BeforeSensitiveProcessor implements Rule {

    @Override
    public String processor(String param) {
        return null;
    }

    /**
     * 全局配置是否是否脱敏的抽象接口，需要调用者去实现，
     * <p>
     * 如果返回false，则不脱敏
     * <p>
     * 如果返回true，则脱敏
     * <p>
     * ps：若是管理员（管理员基本都具有查看敏感数据的权限），则返回 false
     *
     * @return
     */
    public abstract boolean bool();

}
