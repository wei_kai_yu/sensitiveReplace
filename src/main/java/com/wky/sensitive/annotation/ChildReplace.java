package com.wky.sensitive.annotation;
import com.wky.sensitive.enums.DataTypeEnum;
import com.wky.sensitive.rule.Impl.NameRuleImpl;

import java.lang.annotation.*;

/**
 * 子层数据是否脱敏
 * @author weikaiyu
 * @version 1.0
 * @date 2022-02-14 9:41
 */
@Target({ElementType.TYPE, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ChildReplace {

    /**
     * 嵌套脱敏的数据类型
     *
     * @return
     */
    DataTypeEnum dataType() default DataTypeEnum.ENTITY;


    /**
     * 若替换的子元素为 List<String> ，需要声明集合数据采用的替换策略
     *
     * @return
     */
    Class rulePath() default NameRuleImpl.class;
}
