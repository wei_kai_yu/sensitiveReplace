package com.wky.sensitive.annotation;

import com.wky.sensitive.rule.Impl.NameRuleImpl;

import java.lang.annotation.*;

/**
 * 标识在属性上，切面SensitiveCoreBean会读取哪个属性标识了该注解，只有标识了该注解才进行敏感数据替换
 *
 * @author weikaiyu
 * @version 1.0
 * @date 2022-02-11 10:03
 */
@Target({ElementType.TYPE, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Replace {


    /**
     * 替换规则，
     * 提供如下四种：com.hgsoft.zengzhiyingyong.module.sensitive.constant.RuleTypesConstant
     *
     * @return
     */
    Class rulePath() default NameRuleImpl.class;

    /**
     * 描述备注
     *
     * @return
     */
    String description() default "";

}
