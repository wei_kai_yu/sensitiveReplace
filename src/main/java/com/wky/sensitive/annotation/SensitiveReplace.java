package com.wky.sensitive.annotation;

import com.wky.sensitive.enums.DataTypeEnum;

import java.lang.annotation.*;

/**
 * 敏感数据替换的切点注解
 *
 * @author weikaiyu
 * @version 1.0
 * @date 2022-02-11 10:03
 */
@Target({ElementType.PARAMETER, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SensitiveReplace {


    /**
     * 数据类型
     *
     * @return
     */
    DataTypeEnum dataType() default DataTypeEnum.COMMON;

    /**
     * 统一返回体的key,dataType为COMMON时需要填写，默认为 result
     * 如下：
     * {
     * code:200
     * message:'成功',
     * result:[{}]
     * }
     * <p>
     * result即为key的值
     *
     * @return
     */
    String key() default "result";

    /**
     * 数据类型, dataType为COMMON时需要填写，默认是lIST
     *
     * @return
     */
    DataTypeEnum keyDataType() default DataTypeEnum.LIST;


}
